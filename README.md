# anicli-pl

#### Skrypt pozwalający oglądać anime z desu-online.pl przez terminal

Skrypt mocno zainspirowany [ani-cli](https://github.com/pystardust/ani-cli)

Działa na zasadzie manipulowania url przez curl i grep

Zależności:
- bash
- curl
- mpv
