#!/bin/bash

while true
do
    clear
    echo "anicli-pl"
    echo -n "Podaj frazę do wyszukania (x - wstecz): "
    read foo
    if [[ $foo == "x" ]]; then
        exit 0
    fi
    foo=$(echo $foo | tr ' ' '+')

    clear
    bar=$(curl -s https://desu-online.pl/?s=$foo | grep -P '(?<=<a href="https://desu-online.pl/anime/).*?(?=" class="tip" rel=")')

    link=$(echo $bar | grep -oP '(?<=<a href="https://desu-online.pl/anime/).*?(?=/" itemprop="url")')
    title=$(echo $bar | grep -oP '(?<=title=").*?(?=" class="tip")')

    echo "anicli-pl"
    echo "Znaleziono: "

    echo "${title}" | nl -nrz -w1 -s" - " - 
    echo -n "Podaj numer serii (x - wstecz): "
    read NUM
    if [[ $NUM == "x" ]]; then
        continue
    fi

    link=$(echo "${link}" | sed "${NUM}q;d")
    title=$(echo "${title}" | sed "${NUM}q;d")

    while true
    do
        lastodc=$(curl -s https://desu-online.pl/anime/${link}/ | grep -oP '(?<=<span class="epcur epcurlast">Odcinek ).*?(?=</span>)')
        clear
        echo "anicli-pl"
        echo "Wybrano: $title"
        echo "Dostępne odcinki: 1-${lastodc}"
        echo -n "Podaj numer odcinka (x - wstecz): "
        read NUM
        if [[ $NUM == "x" ]]; then
            break
        fi

        bar=$(curl -L -s "https://desu-online.pl/${link}-odcinek-${NUM}"  | grep -oP '(?<=<div class="player-embed" id="pembed">).*?(?=</div>)' | grep -oP '(?<=src=").*?(?=")')

        bar=${bar/'//ok.ru'/'https://ok.ru'}

        clear
        echo "anicli-pl"
        echo "Odtwarzanie: $title odcinek: $NUM"
        echo $bar
        mpv $bar
    done
done